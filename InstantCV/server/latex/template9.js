const fs = require('fs');
const dotenv = require('dotenv');
const transform = require('../functions/transform/transform');


async function JSONtoTEX(jsonData){
    console.log('from JSONtoTEX: ', jsonData);

    let addedLanguages = false;

    let cvColor = jsonData.Color === '' ? `FF0022` : makeColor(jsonData.Color);
    console.log(makeColor(jsonData.Color));
let intro =
    `\\documentclass[11pt,a4paper]{awesome-cv}
    \\geometry{left=1.4cm, top=.8cm, right=1.4cm, bottom=1.8cm, footskip=.5cm}

    \\fontdir[fonts\/]
    \\definecolor{mycolor}{HTML}{${cvColor}}
    \\colorlet{awesome}{mycolor}

    \\renewcommand{\\acvHeaderSocialSep}{\\quad\\textbar\\quad}

    \\name{${jsonData.Name}}{${jsonData.LastName}}
    ${makeAddress(jsonData)}

    \\mobile{${transform.escapeCharacters(jsonData.Phone)}}
    \\email{${transform.escapeCharacters(jsonData.Email)}}
    \\linkedin{${transform.escapeCharacters(jsonData.LinkedIn)}}

    \\begin{document}

    \\makecvheader

    \\makecvfooter
      {\\today}
      {${jsonData.Name}~~~·~~~Curriculum Vitae}
      {\\thepage}

    `;

    let sectionsPart = '';
    const keys = Object.keys(jsonData);

    if(!addedLanguages && jsonData.Languages !==''){
     sectionsPart += makeLanguages(jsonData.Languages) + '\n';
     addedLanguages = true;
}
    for(let i = process.env.NUM_OF_STATIC_FIELDS; i < keys.length; i++){
        let section = keys[i];

        let sectionString = makeSection(section, jsonData[section]);
        sectionsPart += sectionString + '\n';
    }

    texString = intro + sectionsPart + '\\end{document}\n';

    await fs.writeFile(`./templates/template9/${jsonData.Name}${jsonData.LastName}.tex`,
     texString

, function (err) {
   if(err) return console.log(err);
   console.log("Pravljenje tex fajla.");
});
}


function makeSection(section, data){
let retVal = '';
let sectionStart = `\\cvsection{${section.toLowerCase()}}`;

switch(section){
   case 'Education':
       retVal = makeEdu(data);
       break;
   case 'WorkExperience':
       sectionStart = `\\cvsection{work experience}`
       retVal = makeExp(data);
       break;
   case 'Volunteering':
       retVal = makeVolunteering(data);
       break;
   case 'ProfessionalSkills':
       sectionStart = `\\cvsection{professional skills}`
       retVal = makeSkills(data);
       break;
   case 'Projects':
       retVal = makeProjects(data);
      break;
   case 'Publications':
       retVal = makePublications(data);
       break;
   case 'Achievements':
       retVal = makeAchievements(data);
       break;
   case 'Talks':
       retVal = makeTalks(data);
       break;
   case 'Courses':
       retVal = makeCourses(data);
       break;
   case 'Interests':
       retVal = makeInterests(data);
       break;
   case 'Hobbies':
       retVal = makeHobbies(data);
       break;
   }

   return sectionStart + '\n' +retVal;
}

function makeAddress(data){
let array = [];

if(data.Address !== ''){
 array.push(` ${data.Address}`);
}
if(data.City !== ''){
 array.push(`${data.City}`);
}
if(data.Country !== ''){
 array.push(`${data.Country}`);
}

return array.length > 0 ? `\\address{${transform.escapeCharacters(data.Address)} - ${transform.escapeCharacters(data.City)}, ${transform.escapeCharacters(data.Country)}}` : '';
}


function makeEdu(education){

let eduList = '';
const keys = Object.keys(education);

for(let key in keys){
   eduList += entry(transform.escapeCharacters(education[keys[key]].year),
    transform.escapeCharacters(education[keys[key]].school),
                    transform.escapeCharacters(education[keys[key]].description),
                    '') + `\\\\`;
}

return `\\begin{cventries}` + eduList + `\\end{cventries}`;
}

function makeAchievements(achievements){

let achievementsList = '';
const keys = Object.keys(achievements);

for(let key in keys){
   achievementsList += entry(transform.escapeCharacters(achievements[keys[key]].year),
    transform.escapeCharacters(achievements[keys[key]].name),
      transform.escapeCharacters(achievements[keys[key]].description),
       '') + `\\\\`;
}

return `\\begin{cventries}` + achievementsList + `\\end{cventries}`;
}
function makeCourses(courses){

let coursesList = '';
const keys = Object.keys(courses);

for(let key in keys){
   coursesList += entry(transform.escapeCharacters(courses[keys[key]].year),
                        transform.escapeCharacters(courses[keys[key]].name),
                        '',
                        '') + `\\\\`;
}

return `\\begin{cventries}` + coursesList + `\\end{cventries}`;
}

function makeExp(experiences){

let expList = '';
let keys = Object.keys(experiences);

for(let key in keys){
   expList += entry(transform.escapeCharacters(experiences[keys[key]].year),
                    transform.escapeCharacters(experiences[keys[key]].company),
                    transform.escapeCharacters(experiences[keys[key]].position),
                    transform.escapeCharacters(experiences[keys[key]].description)) + `\\\\`;
}

return `\\begin{cventries}` + expList + `\\end{cventries}`;
}

function makeHobbies(hobbies){
const keys = Object.keys(hobbies);

return `\\\\` + transform.escapeCharacters(hobbies[keys[0]].description) + `\\\\ \n`;
}

function makeInterests(interests){
const keys = Object.keys(interests);

return `\\\\` + transform.escapeCharacters(interests[keys[0]].description) + `\\\\ \n`;
}

function makeProjects(projects){

let projectsList = '';
const keys = Object.keys(projects);

for(let key in keys){
   projectsList += entry(transform.escapeCharacters(projects[keys[key]].year),
                         transform.escapeCharacters(projects[keys[key]].name),
                         transform.escapeCharacters(projects[keys[key]].type),
                         transform.escapeCharacters(projects[keys[key]].description)) + `\\\\`;
}

return `\\begin{cventries}` + projectsList + `\\end{cventries}`;
}

function makePublications(publications){

let publicationsList = '';
const keys = Object.keys(publications);

for(let key in keys){
   publicationsList += entry(transform.escapeCharacters(publications[keys[key]].year),
                             transform.escapeCharacters(publications[keys[key]].name),
                             transform.escapeCharacters(publications[keys[key]].description),
                             '') + `\\\\`;
}

return `\\begin{cventries}` + publicationsList + `\\end{cventries}`;
}

function makeSkills(skills){

let skillArray = [];
let keys = Object.keys(skills);

for(let key in keys){
   skillArray.push(transform.escapeCharacters(skills[keys[key]].skill));
}

return `\\\\` + `\n` + skillArray.join(', ') + `\\\\` + `\n`;
}

function makeTalks(talks){
let talksList = '';
const keys = Object.keys(talks);

for(let key in keys){
   talksList += entry(transform.escapeCharacters(talks[keys[key]].year), transform.escapeCharacters(talks[keys[key]].description), '', '') + `\\\\`;
}

return `\\begin{cventries}` + talksList + `\\end{cventries}`;
}

function makeVolunteering(volunteering){
let volsList = '';
const keys = Object.keys(volunteering);

for(let key in keys){
   volsList += entry(transform.escapeCharacters(volunteering[keys[key]].year),
                     transform.escapeCharacters(volunteering[keys[key]].company),
                     transform.escapeCharacters(volunteering[keys[key]].description),
                     '') + `\\\\`;
}

return `\\begin{cventries}` + volsList + `\\end{cventries}`;
}

function makeLanguages(languages){

let section = '\\cvsection{languages}\n';

if(languages !== ''){

   section += '\\begin{itemize}\n';
   langs = languages.split(', ');

   for(let lang in langs){
       language = langs[lang].split(' ');
       section += `\\item ${transform.escapeCharacters(language[0])} - ${transform.escapeCharacters(language.slice(1).join(' '))} \n`;
   }
   section += '\\end{itemize}\n';
}

return section;
}

function entry(param1, param2, param3, param4){
  let param4String = param4 !== '' ? `\\begin{cvitems}\\item{${param4}}\\end{cvitems}` : '';

  return `\\cventry{${param3}}{${param2}}{${param1}}{}{${param4String}}\n`;
}

function makeColor(data){
  return data.toUpperCase().substr(1, 7);
}

module.exports = {JSONtoTEX};
