const fs = require('fs');
const transform = require('../functions/transform/transform');




async function JSONtoTEX(jsonData){
    console.log('from JSONtoTEX: ', jsonData);
    let cvColor = jsonData.Color === '' ? {r: 255, g: 150, b:0} : transform.hexToRgb(jsonData.Color);

    let header =
    `\\documentclass[10pt,A4]{instantcv}
    \\begin{document}

    \\setcol{${cvColor.r}, ${cvColor.g}, ${cvColor.b}}
    \\pagestyle{fancy}
    \\vspace{-20.55pt}
    \\hspace{-0.25\\linewidth}\\colorbox{bgcol}{\\makebox[1.5\\linewidth][c]{\\HUGE{\\textcolor{white}{\\textsc{${transform.escapeCharacters(jsonData.Name)} ${transform.escapeCharacters(jsonData.LastName)}}} }} }

    `

    let profilePic = '';

    if(jsonData.Picture)
      profilePic = makeProfilePic(jsonData.Picture);
    else
      profilePic = '\\vspace{0.5cm}\n'

    let meta = 4;

    let address = city = country = email = phone = linkedin = '';

    if(jsonData.Address !== ''){
      address = metaSection('Address', jsonData.Address);
      meta -= 1;
    }

    if(jsonData.City !== ''){
      city = metaSection('City', jsonData.City);
      meta -= 1;
    }

    if(jsonData.Country !== ''){
      country = metaSection('Country', jsonData.Country);
      meta -= 1;
    }

    if(jsonData.Phone!== ''){
      phone = metaSection('Phone', jsonData.Phone);
      meta -= 1;
    }

    if(jsonData.Email!== ''){
      email = metaSection('Email', jsonData.Email);
      meta -= 1;
    }

    if(jsonData.LinkedIn!== ''){
      linkedin = metaSection('LinkedIn', jsonData.LinkedIn);
      meta -= 1;
    }

    let vspace = 0;
    console.log('Meta: ', meta);
    if(meta <= 0){
      vspace = 0.5;
    }
    else{
      vspace = 4.5 - meta;
    }


    let metaSections =
    `${profilePic}

    ${address}
    ${city}
    ${country}
    ${email}
    ${phone}
    ${linkedin}
    \\vspace{${vspace}cm}\n
    `

    let sections = '';
    let other = false;
    const keys = Object.keys(jsonData);
    for(let i = process.env.NUM_OF_STATIC_FIELDS; i < keys.length; i++){
        let section = keys[i];

        if((section === 'Interests' || section === 'Hobbies') && !other){
          sections += makeLanguages(jsonData.Languages);
          sections += makeOther(section ,jsonData);
          other = true;
          continue;
        }

        if(section === 'Interests' || section === 'Hobbies')
          continue;

        let sectionString = makeSection(section, jsonData[section]);
        sections += sectionString + '\n';
    }

    let texString = header + metaSections + sections + '\\end{document}';
    await fs.writeFile(`./templates/template4/${jsonData.Name}${jsonData.LastName}.tex`,
     texString, function (err) {
        if(err) return console.log(err);
        console.log("Pravljenje tex fajla.");
    });

}


function makeSection(section, data){
  let retVal = '';
  let sectionStart = `\\cvsection{${section}}\n`;

  switch(section){
      case 'Education':
          retVal = makeEdu(data);
          break;
      case 'WorkExperience':
          sectionStart = `\\cvsection{Work experience}\n`
          retVal = makeExp(data);
          break;
      case 'Volunteering':
          retVal = makeVolunteering(data);
          break;
      case 'ProfessionalSkills':
          sectionStart = `\\cvsection{Professional skills}\n`
          retVal = makeSkills(data);
          break;
      case 'Projects':
          retVal = makeProjects(data);
         break;
      case 'Publications':
          retVal = makePublications(data);
          break;
      case 'Achievements':
          retVal = makeAchievements(data);
          break;
      case 'Talks':
          retVal = makeTalks(data);
          break;
      case 'Courses':
          retVal = makeCourses(data);
          break;
      }

      return sectionStart + '\n' +retVal;
}

function makeAddress(address, city, country){
}

function makePhone(phone){
}

function makeMail(email){
}

function makeLinkedIn(linkedIn){
}

function makeLanguages(languages){
  let languagesString = '';

    if(languages != ''){
        let langs = languages.split(", ");

        let i;
        languagesString =
        `\\cvsection{Languages}\n
        `;
        for(i=0; i < langs.length; i++){
            let current = langs[i].split(" ");
            languagesString +=
            `\\cvlang{${transform.escapeCharacters(current[0])}}{${transform.escapeCharacters(current.slice(1).join(' '))}}\n`;
        }
  }
    return languagesString;
}

function makeEdu(education){
  let eduList = '';
    const keys = Object.keys(education);

    for(let key in keys){
        eduList += cvEventThree(education[keys[key]].year,
                                education[keys[key]].school,
                                education[keys[key]].description);
    }

    return eduList;
}


function makeExp(experience){
  let expList = '';
  const keys = Object.keys(experience);

    for(let key in keys){
        expList += `\\experienceevent{${transform.escapeCharacters(experience[keys[key]].year)}}
        {${transform.escapeCharacters(experience[keys[key]].company)}}
        {${transform.escapeCharacters(experience[keys[key]].position)}}
        {${transform.escapeCharacters(experience[keys[key]].description)}}\n`
    }

    return expList;

}

function makeProjects(projects){
    let projList = '';
    const keys = Object.keys(projects);

    for(let key in keys){
        projList += `\\projectevent{${transform.escapeCharacters(projects[keys[key]].year)}}
                          {${transform.escapeCharacters(projects[keys[key]].name)}}
                          {${transform.escapeCharacters(projects[keys[key]].type)}}
                          {${transform.escapeCharacters(projects[keys[key]].description)}}\n`
    }

    return projList;
}

function makeVolunteering(volunteering){
  let volsList = '';
    const keys = Object.keys(volunteering);

    for(let key in keys){
        volsList += cvEventThree(volunteering[keys[key]].year,
                                 volunteering[keys[key]].company,
                                 volunteering[keys[key]].description);
    }

    return volsList;
}

function makeAchievements(achievements){

  let achievementsList = '';
  const keys = Object.keys(achievements);

  for(let key in keys){
      achievementsList += cvEventThree(achievements[keys[key]].year,
                                       achievements[keys[key]].name,
                                       achievements[keys[key]].description,);
  }

  return achievementsList;

}


function makeSkills(skills){
  let skillArray = [];
  let keys = Object.keys(skills);

  for(let key in keys){
      skillArray.push(transform.escapeCharacters(skills[keys[key]].skill));
  }

  return `\\cveventtwo{}{${skillArray.join(', ')}}`;
}


function makePublications(publications){
  let publicationsList = '';
  const keys = Object.keys(publications);

  for(let key in keys){
      publicationsList += cvEventThree(publications[keys[key]].year,
                                publications[keys[key]].name,
                                publications[keys[key]].description);
  }

  return publicationsList;
}

function makeTalks(talks){

  let talksList = '';
  const keys = Object.keys(talks);

  for(let key in keys){
      talksList += cvEventTwo(talks[keys[key]].year, talks[keys[key]].description);
  }

  return talksList;
}

function makeCourses(courses){
  let coursesList = '';
    const keys = Object.keys(courses);

    for(let key in keys){
        coursesList += cvEventTwo(courses[keys[key]].year,
                                  courses[keys[key]].name);
    }

    return coursesList;
}

function makeOther(key,data){
  let retval = `\\cvsection{Other info}\n\n`;
  if(key === 'Interests'){
    const keysInterests = Object.keys(data.Interests);

    retval += `\\cvother{Interests}{${transform.escapeCharacters(data.Interests[keysInterests[0]].description)}}\n`;
    if(data.Hobbies){
      const key = Object.keys(data.Hobbies);
      retval += `\\cvother{Hobbies}{${transform.escapeCharacters(data.Hobbies[key[0]].description)}}\n`;
    }
  }
  else{
    const keysHobbies = Object.keys(data.Hobbies);

    retval += `\\cvother{Hobbies}{${transform.escapeCharacters(data.Hobbies[keysHobbies[0]].description)}}\n`;
    if(data.Interests){
      const key = Object.keys(data.Interests);
      retval += `\\cvother{Interests}{${transform.escapeCharacters(data.Interests[key[0]].description)}}\n`;
    }
  }
  return retval;
}

function metaSection(section, data){
  return `\\metasection{${section}}{${transform.escapeCharacters(data)}}\n`;
}

function makeProfilePic(path){
  return `\\profilepic{${path}}\n\\vspace{-4.5cm}\n`;
}

function cvEventThree(param1, param2, param3){
  return `\\cveventthree{${transform.escapeCharacters(param1)}}{${transform.escapeCharacters(param2)}}{${transform.escapeCharacters(param3)}}\n`;
}

function cvEventTwo(param1, param2){
  return `\\cveventtwo{${transform.escapeCharacters(param1)}}{${transform.escapeCharacters(param2)}}`
}
module.exports = {JSONtoTEX};
