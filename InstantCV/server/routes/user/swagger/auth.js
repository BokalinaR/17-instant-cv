/** 
* @swagger
* tags:
* - name: "User Authentication"
*   description: "How to register and login"
*paths:
*  /api/user/register:
*    post:
*      tags:
*      - "User Authentication"
*      summary: "Register user"
*      description: "Create new user with name,password and unique email"
*      consumes:
*      - "application/json"
*      produces:
*      - "application/json"
*      parameters:
*      - in: "body"
*        name: "body"
*        description: "New user information"
*        required: true
*        schema:
*          type: "object"
*          required:
*          - "name"
*          - "email"
*          - "password" 
*          properties:
*           name:
*               type: string
*               example: "Student" 
*           email:
*               type: string
*               example: "student@gmail.com"
*           password:
*               type: string
*               example: "1234567"
*      responses:
*       400:
*          description: "Email already exists"
*       200:
*          schema:
*           type: "object"
*           required:
*           - "name"
*           - "_id" 
*           properties:
*               _id:
*                   type: string
*                   example: "5e9628c846cf53163ce48586" 
*               name:
*                   type: string
*                   example: "Student"
*/

/** 
* @swagger
*paths:
*  /api/user/login:
*    post:
*      tags:
*      - "User Authentication"
*      summary: "Login user"
*      description: "Login user with email and password. Return JWT in header as 'auth-token' and 'Logged in!'"
*      consumes:
*      - "application/json"
*      produces:
*      - "application/json"
*      parameters:
*      - in: "body"
*        name: "body"
*        description: "New user information"
*        required: true
*        schema:
*          type: "object"
*          required:
*          - "email"
*          - "password" 
*          properties:
*           email:
*               type: string
*               example: "student@gmail.com"
*           password:
*               type: string
*               example: "1234567"
*      responses:
*       400:
*          description: "'Email already exists' OR 'Invalid password'"
*       200:
*          description: "Logged in!"
*          headers:
*           auth-token:
*               schema:
*                   type: string
*                   example: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9..."
*               description: JWT authentication token.
*/