const router = require('express').Router();
const controller = require('../../controllers/userController');

//api/user/update
//**all API's are verified before with middleware function from verifyToken.js
//required header parameter auth-token (received from login request)

router.get('/info', controller.updateInfo);

router.get('/delete', controller.deleteAcc);

router.post('/email', controller.updateEmail);

router.post('/name', controller.updateName);

router.post('/password', controller.updatePass);

router.post('/change_pic/:userId', controller.updateProfilePic)

module.exports = router;