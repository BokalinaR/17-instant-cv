import { Component, OnInit, Injectable } from '@angular/core';
import { SafeResourceUrl, DomSanitizer } from '@angular/platform-browser';
import { AuthService } from '../services/auth.service';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Router } from '@angular/router';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
@Injectable()
export class ProfileComponent implements OnInit {

  public safePicturePath: SafeResourceUrl;
  public safeCvPaths: SafeResourceUrl[] = [];

  constructor( private _router: Router,
               private http: HttpClient,
               private auth_service: AuthService,
               private sanitizer: DomSanitizer) { }

  ngOnInit(): void {
    let userId = this.auth_service.getID();
    
    if(!userId)
      this._router.navigateByUrl("/choose-template");
    else{
      this.http.get(`${environment.serverUrl}/user/${userId}`)
                    .subscribe(data=>{
                      document.getElementById("name").innerHTML = data['name'];
                      document.getElementById("email").innerHTML = data['email'];
                      document.getElementById("date").innerHTML = 'Member since: ' 
                                            + new Date(Date.parse(data['date'])).toLocaleString();
                      this.safePicturePath = data['pic'] ?
                      this.sanitize(`${environment.serverUrl}/user/img/${data['pic']}`) : 
                      this.sanitize(`${environment.serverUrl}/user/img/profile.png`);
                      
                      data['_cv'].forEach(element => {
                        let path = `${environment.serverUrl}/images/${element['path']}`;
                        this.safeCvPaths.push(this.sanitize(path));
                      });
                    });
    }
    this.auth_service.updateTokenExpiry('auth-token');
  }


  sanitize(path){
    return this.sanitizer.bypassSecurityTrustResourceUrl(path);
  }

  private reload(id){
    let container = document.getElementById(id);
    let content = container.innerHTML;
    container.innerHTML= content;

   //this line is to watch the result in console , you can remove it later
    console.log("Refreshed"); 
  }
}
