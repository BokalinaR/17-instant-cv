import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  constructor(private _router: Router) { }

  public setTokenWithExpiry(key, value, ttl) {
    const now = new Date();

    const item = {
      value: value,
      expiry: now.getTime() + ttl
    }
    localStorage.setItem(key, JSON.stringify(item));
  }

  public getWithExpiry(key) {
    const itemStr = localStorage.getItem(key)
    if (!itemStr) {
      return null
    }
    const item = JSON.parse(itemStr)
    const now = new Date()
    
    if (now.getTime() > item.expiry) {
      // If the item is expired, delete the item from storage
      localStorage.removeItem(key);
      localStorage.removeItem('userID');
      return null
    }
    return item.value
  }
  
  public updateTokenExpiry(key) {
    const itemStr = localStorage.getItem(key)
    if (!itemStr) {
      return null
    }
    const item = JSON.parse(itemStr)
    const now = new Date()
    
    if (now.getTime() > item.expiry) {
      // If the item is expired, delete the item from storage
      localStorage.removeItem(key);
      //localStorage.removeItem('userID');
      return null
    }
    else if(item.expiry - now.getTime() < 900000){
      item.expiry = now.getTime() + 2000000;
    }
    localStorage.setItem('auth-token', JSON.stringify(item));
  }

  public setTokenHeader() {
    let headers: HttpHeaders = new HttpHeaders();
    headers = headers.set('auth-token',this.getWithExpiry('auth-token'));
    //console.log(headers);
    return headers;
  }

  public setID(newID: string){
    localStorage.setItem('userID', newID);
  }

  public getID(){
    const item = localStorage.getItem('userID');
       
    if(!item)
      return null;
    return item;
  }

  public logout() {
    localStorage.removeItem("auth-token");
    localStorage.removeItem('userID');
    this._router.navigateByUrl("/");
  }
}
