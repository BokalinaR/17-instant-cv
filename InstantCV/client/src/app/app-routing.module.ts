import { NgModule }              from '@angular/core';
import { Routes, RouterModule }  from '@angular/router';
import { FormComponent }         from './form/form.component';
import { TemplatesComponent }    from './templates/templates.component';
import { EditTemplateComponent } from './edit-template/edit-template.component';
import { AboutComponent }        from './about/about.component';
import { ProfileComponent }      from './profile/profile.component';
import { AccountComponent }      from './account/account.component';
import { ImageCropperComponent } from './image-cropper/image-cropper.component';
import { PasswordComponent }     from './password/password.component';
import { ValidationComponent }     from './validation/validation.component';

/* Routing:  */
const routes: Routes = [
  // https://localhost:4200/
  {path: '', component: FormComponent},
  // https://localhost:4200/choose-template
  {path: 'choose-template', component: TemplatesComponent},
  // https://localhost:4200/template/ 1 2 3 ...
  {path: 'template/:templateId', component: EditTemplateComponent},
  // https://localhost:4200/template/about
  {path: 'about', component: AboutComponent},
  // https://localhost:4200/profile
  {path: 'profile', component: ProfileComponent},
  // https://localhost:4200/account
  {path: 'account', component: AccountComponent},
  // https://localhost:4200/profile/change_pic
  {path: 'change_pic', component: ImageCropperComponent},
  // https://localhost:4200/forgot-password
  {path: 'forgot-password', component: PasswordComponent},
  // https://localhost:4200/validate-account
  {path: 'validate-account', component: ValidationComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
