# Instant CV :page_facing_up:

InstantCV is client/server app that aims to create an appropriate CV for an user based on user's design wishes. 

The user has two options:
- To make her/his own account;
- To continue as a guest.

In the first case, the user is able to save the previous work (while the guest is not) and manage the account.

Also, the user is able to preview her/his CV at any time, before submitting information and downloading the zip file. 
The zip file consists of pdf file and a group of files written in latex, which are necessary for optional subsequent editing.

### Client - How to run
1. Install node modules with command: `npm install`
2. Run `ng serve` for a dev server
3. Navigate to `http://localhost:4200`.

This part was generated with Angular CLI version 9.1.1.  To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

### Server - How to run
1. Install node modules with command: `npm install`
2. Run server: `npm start`


## Developers

- [Marija Markovic, 76/2016](https://gitlab.com/marija.markovic)
- [Lea Petkovic, 163/2016](https://gitlab.com/leic25)
- [Nikola Stamenic, 177/2016](https://gitlab.com/stuckey10)
- [Bojana Ristanovic, 45/2016](https://gitlab.com/BokalinaR)
- [Nikola Stojevic, 1111/2018](https://gitlab.com/nikolastojevic)

![LogInPage](InstantCV/screenshots/seventhweek03.png)